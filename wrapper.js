var child_process = require("child_process");
var Discord = require("discord.js");

var discordBot = new Discord.Client();
discordBot.on("error", console.error);
discordBot.login(process.cwd().includes("creative") ? "" : "");
discordBot.on("ready", () => {
	discordBot.user.setActivity(process.cwd().includes("creative") ? "creative.mc.jpdld.com" : process.cwd().includes("survival") ? "survival.mc.jpdld.com" : undefined);
});

function send(message) {
	var channel = discordBot.channels.get("452025433328975872");
	if (channel) channel.send(message, {split:{char:''}});
}

var minecraftServer;
(function startMinecraftServer() {
	console.log("Starting Minecraft Server…");
	minecraftServer = child_process.spawn("java", ["-Xmx2G", "-jar", "server.jar"]);
	minecraftServer.stdout.pipe(process.stdout);
	minecraftServer.stderr.pipe(process.stderr);
	process.stdin.pipe(minecraftServer.stdin);

	var enableInfo = false;
	minecraftServer.stdout.on('data', (data) => {
		data = String(data).trim();
		data = data.split("\n");
		data.forEach(data => {
			var indexOfInfo = data.indexOf("[Server thread/INFO]: ");
			if (indexOfInfo != -1) {
				var info = data.substr(indexOfInfo + 22);
				if (info.startsWith("Done (") && info.endsWith(')! For help, type "help"')) {
					enableInfo = true;
					send("✅ **Server has started**");
				} else if (info == "Stopping server") {
					enableInfo = false;
					//send("🛑 **Server has stopped**");
				} else {
					if (!enableInfo) return; // only start sending info after all the startup stuff is finished
					if (info.includes("] logged in with entity id ")) return; // ignore technical login messages
					if (info.includes(" lost connection: ")) return; // ignore technical disconnect messages
					if (info.startsWith("Disconnecting")) return; // ignore connection rejects
					if (info.startsWith("[") && info.endsWith("]")) return; // ignore op broadcasts
					if (info == "Scanning for legacy world dragon fight...") return;
					if (info == "Found that the dragon has not yet been killed in this world.") return;
					if (info.startsWith("Found that there's a dragon still alive")) return;
					if (info == "Found that the dragon has been killed in this world already.") return;
					if (info.startsWith("ThreadedAnvilChunkStorage")) return; // only seen this during shutdown but just in case
					//if (info == "Unknown command") return;
					//if (info.endsWith("<--[HERE]")) return;
					if (info == "No player was found") return; // caused by tellraw when nobody online
					if (info == "Stopping the server") return; // this is response to "stop" command
					send(info);
				}
			} else if (data.includes("[Server Watchdog/") || data.includes("[Server Thread/ERROR]")) {
				send(data.substr(data.indexOf("]: ") + 3));
			}
		});
	});

	minecraftServer.on("close", () => {
		minecraftServer = undefined;
		send("🛑 **Server has stopped**\n_Restarting in 10 seconds…_");
		console.log("Restarting in 10 seconds…");
		setTimeout(startMinecraftServer, 10000);
	});
})();

discordBot.on("message", function(message){
	if (message.channel.id != "452025433328975872") return;
	if (message.author.id == discordBot.user.id) return;
	if (minecraftServer) {
		let words = message.content.split(' ');
		if ((words[0] == "!cc" && process.cwd().includes("creative")) || (words[0] == "!cs" && process.cwd().includes("survival")) || words[0] == "!c") {
			let roles = message.member.roles.map(r => r.name.toLowerCase());
			if (message.member && (roles.includes("minecraft admin") || roles.includes("minecraft mod"))) {
				minecraftServer.stdin.write(words.slice(1).join(' ') + '\n');
			} else {
				message.react('🚫');
			}
		} else if (words[0] == "!list") {
			minecraftServer.stdin.write("list\n");
		} else {
			let text = `[D] <${message.member && message.member.displayName || message.author.username}> ${message.cleanContent}`;
			if (message.attachments.size > 0) text += ' ' + message.attachments.map(a => a.url).join(' ');
			minecraftServer.stdin.write(`tellraw @a ${JSON.stringify({text})}\n`);
		}
	}
});
